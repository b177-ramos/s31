
// Module -> Controller -> Routes -> End Point
// Models contains what object are needed in our API. Serves as an entity that contains the structure of the model
// Model example:Students need name, course, subjects etc..
const mongoose = require("mongoose");

// Create the schema and model
const taskSchema = new mongoose.Schema({
	name : String,
	status : {
		type : String,
		default : "pending"
	}
});

// Export the file
module.exports = mongoose.model("Task", taskSchema);