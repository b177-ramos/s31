
// Module -> Controller -> Routes -> End Point
const express = require("express");

// Create a Router instance that functions as a routing system
const router = express.Router();

// Import the taskControllers
const taskController = require("../controllers/taskControllers");


// Route get all the tasks
// Endpoint: localhost:3001/tasks -> /getTask
router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

// Route to Create a task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});

// Route to delete a Task
// endpoint: localhost:3001/tasks/"idNum"
router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

// Route to update a task
router.put("/:id", (req, res) => {
	// Update by the id, the body will store the update
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

//
router.put("/:id/complete", (req, res) => {
	taskController.updateTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// 
// Export the router object to be used in index.js
module.exports = router;
