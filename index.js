// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute");

// Server setup9
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Database connection
mongoose.connect("mongodb+srv://admin:12345@cluster0.p2t9q.mongodb.net/b177-to-do?retryWrites=true&w=majority", {
// To avoid current and future errors in connection
	useNewUrlParser : true,
	useUnifiedTopology : true
});

app.use("/tasks", taskRoute);

// Server listening
app.listen(port, () => console.log(`Now listening to port ${port}`));